import java.io.*;

/**
 * Beschreiben Sie hier die Klasse geburtstag.
 * 
 * @author Frank
 * @version 0.1
 */
public class geburtstag
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private String binInput;

    /**
     * Konstruktor für Objekte der Klasse geburtstag
     */
    public geburtstag() throws IOException
    {
        String binInput;
        try(BufferedReader br = new BufferedReader(new FileReader("geb.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                //sb.append(System.lineSeparator());
                sb.append(" ");
                line = br.readLine();
            }
             binInput = sb.toString();
        }// Instanzvariable initialisieren
        this.binInput = binInput;
    }

    /**
     * Untersuchung
     */
    public void untersuche() 
    {
        StringBuilder output = new StringBuilder();
        String[] inChars = this.binInput.split("\\s");
        
        for (int i=0; i<inChars.length; i++) {
            int dec = Integer.parseInt(inChars[i],2);
            char c;
            if ( dec < 4) {
                c = (char) (dec + 48);
            } else {
                c = (char) dec;
            }
            output.append(c);
            //System.out.println(inChars[i] + "  " + dec + " " + c);
        }
        
        // tragen Sie hier den Code ein
        System.out.println(output.toString());
    }
}
